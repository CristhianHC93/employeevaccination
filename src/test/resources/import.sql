/* CREATE USER ADMIN */
INSERT INTO users (username, password,identification_card, email, status,first_name,last_name,birth_date,create_at) VALUES ('admin','$2a$10$kzijx18JjU/eYH7KrMnW8O3ZS76bLAaFVBk.ddj4SsxUtBMbsHwaC','1313199067', 'demo@demo.com',1, 'admin 1','demo 1','1993/10/6','2021/7/3');
INSERT INTO roles (authority) VALUES ('ROLE_USER');
INSERT INTO roles (authority) VALUES ('ROLE_ADMIN');
INSERT INTO user_role (user_id,role_id) VALUES (1,2);