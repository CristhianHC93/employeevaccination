package ec.cristhian.holguin.employee_vaccination.controllers;

import static org.hamcrest.Matchers.*;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.*;

import ec.cristhian.holguin.employee_vaccination.auth.service.JWTService;
import ec.cristhian.holguin.employee_vaccination.models.data.VaccineType;
import ec.cristhian.holguin.employee_vaccination.models.dto.CreateUserDto;
import ec.cristhian.holguin.employee_vaccination.models.dto.UpdateUserDto;
import ec.cristhian.holguin.employee_vaccination.models.dto.VaccinationDto;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class UserControllerTest {

	@Autowired
	private JWTService jwtService;

	@Autowired
	private WebTestClient webTestClient;

	private String token;
	private static String id;

	@BeforeEach
	void setup() {
		try {
			token = jwtService.createTokenTest("admin");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	@Order(1)
	void testCreateUserBadRequest() throws Exception {
		CreateUserDto createUserDto = new CreateUserDto();
		createUserDto.setEmail("demo");
		createUserDto.setFirstName("Juan");
		createUserDto.setIdentificationCard("123456789");
		createUserDto.setLastName("");
		webTestClient.post().uri("http://localhost:8080/api/users").contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + token).bodyValue(createUserDto).exchange().expectStatus()
				.isBadRequest().expectBody().jsonPath("$.identificationCard")
				.value(is("length must be 10 and only allow numbers")).jsonPath("$.email")
				.value(is("must be a well-formed email address")).jsonPath("$.lastName").value(is("must not be empty"));
	}

	@Test
	@Order(2)
	void testCreateUserOk() throws Exception {
		CreateUserDto createUserDto = new CreateUserDto();
		createUserDto.setEmail("demo@demo.com");
		createUserDto.setFirstName("Juan");
		createUserDto.setIdentificationCard("1234567890");
		createUserDto.setLastName("Perez");

		webTestClient.post().uri("http://localhost:8080/api/users").contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + token).bodyValue(createUserDto).exchange().expectStatus().isOk()
				.expectBody().jsonPath("$.id").value(data -> UserControllerTest.id = data.toString())
				.jsonPath("$.username").isNotEmpty();
	}

	@Test
	@Order(3)
	void testUpdateUserBadRequest() throws Exception {
		UpdateUserDto updateUserDto = new UpdateUserDto();
		updateUserDto.setEmail("demo@demo.com");
		updateUserDto.setFirstName("Juan");
		updateUserDto.setLastName("Perez");
		updateUserDto.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").parse("1993-10-05"));
		updateUserDto.setLastName("Perez");
		updateUserDto.setAddress("SD");
		updateUserDto.setMobile("0908012165");
		updateUserDto.setVaccinationStatus(1);
		webTestClient.put().uri("http://localhost:8080/api/users/" + id.toString())
				.contentType(MediaType.APPLICATION_JSON).header("Authorization", "Bearer " + token)
				.bodyValue(updateUserDto).exchange().expectStatus().isBadRequest().expectBody().jsonPath("$.message")
				.value(is("Vaccination information is necessary."));
	}

	@Test
	@Order(4)
	void testUpdateUserBadRequestVaccination() throws Exception {
		UpdateUserDto updateUserDto = new UpdateUserDto();
		updateUserDto.setEmail("demo@demo.com");
		updateUserDto.setFirstName("Juan");
		updateUserDto.setLastName("Perez");
		updateUserDto.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").parse("1993-10-05"));
		updateUserDto.setAddress("SD");
		updateUserDto.setMobile("0908012165");
		updateUserDto.setVaccinationStatus(1);
		VaccinationDto vaccinationDto = new VaccinationDto();
		vaccinationDto.setNumberDoses(1);
		vaccinationDto.setVaccinationDate(new Date());
		vaccinationDto.setVaccineType(VaccineType.PFIZER);
		updateUserDto.setVaccination(vaccinationDto);

		webTestClient.put().uri("http://localhost:8080/api/users/" + id.toString())
				.contentType(MediaType.APPLICATION_JSON).header("Authorization", "Bearer " + token)
				.bodyValue(updateUserDto).exchange().expectStatus().isOk().expectBody().jsonPath("$.vaccination_status")
				.value(is(1));
	}

	@Test
	@Order(5)
	void testDeleteUserBadRequest() throws Exception {
		webTestClient.delete().uri("http://localhost:8080/api/users/66").header("Authorization", "Bearer " + token)
				.exchange().expectStatus().isNotFound().expectBody().jsonPath("$.message")
				.value(is("Unable to find ec.cristhian.holguin.employee_vaccination.models.entity.User with id 66"));
	}

	@Test
	@Order(6)
	void testDeleteUserOk() throws Exception {
		webTestClient.delete().uri("http://localhost:8080/api/users/" + id.toString())
				.header("Authorization", "Bearer " + token).exchange().expectStatus().isOk().expectBody()
				.jsonPath("$.id").isNumber().jsonPath("$.username").isNotEmpty();
	}

}
