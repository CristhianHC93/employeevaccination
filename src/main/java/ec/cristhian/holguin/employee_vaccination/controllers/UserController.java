package ec.cristhian.holguin.employee_vaccination.controllers;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ec.cristhian.holguin.employee_vaccination.exception.exceptions.BadRequestException;
import ec.cristhian.holguin.employee_vaccination.models.data.RoleName;
import ec.cristhian.holguin.employee_vaccination.models.data.VaccinationStatus;
import ec.cristhian.holguin.employee_vaccination.models.data.VaccineType;
import ec.cristhian.holguin.employee_vaccination.models.dto.CreateUserDto;
import ec.cristhian.holguin.employee_vaccination.models.dto.UpdateUserDto;
import ec.cristhian.holguin.employee_vaccination.models.dto.UserDto;
import ec.cristhian.holguin.employee_vaccination.models.dto.UsersDto;
import ec.cristhian.holguin.employee_vaccination.models.entity.User;
import ec.cristhian.holguin.employee_vaccination.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@Tag(name = "User")
@RequestMapping("/api/users")
public class UserController {

	private UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	@Operation(security = @SecurityRequirement(name = "bearer-key"))
	@PostMapping()
	@ApiResponse(responseCode = "200", description = "Return a user with username and password equals to identification_card", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UserDto.class)))
	public UserDto createUser(@Valid @RequestBody CreateUserDto createUserDto) {
		return userService.createUser(createUserDto);
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	@Operation(security = @SecurityRequirement(name = "bearer-key"))
	@PutMapping("/{id}")
	@ApiResponse(responseCode = "200", description = "Updating user for admin", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UserDto.class)))
	public UserDto updateUserForAdmin(@Valid @RequestBody UpdateUserDto updateUserDto, @PathVariable Long id) {
		System.out.println(updateUserDto.getBirthDate());
		return userService.updateUser(updateUserDto, id);
	}

	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
	@Operation(security = @SecurityRequirement(name = "bearer-key"))
	@PutMapping()
	@ApiResponse(responseCode = "200", description = "Updating user for employee, only can update yours data", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UserDto.class)))
	public UserDto updateUserForUser(HttpServletRequest request, @Valid @RequestBody UpdateUserDto updateUserDto) {
		return userService.updateUser(updateUserDto, ((User) request.getAttribute("user")).getId());
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	@Operation(security = @SecurityRequirement(name = "bearer-key"))
	@GetMapping(value = "/employee")
	@ApiResponse(responseCode = "200", description = "Users found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UsersDto.class)))
	public UsersDto getUsersEmployee(@RequestParam(required = false, defaultValue = "0") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit) {
		return userService.getUserByRole(RoleName.ROLE_USER, (page != null) ? page : 0, (limit != null) ? limit : 10);
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	@Operation(security = @SecurityRequirement(name = "bearer-key"))
	@GetMapping(value = "/vaccination/status/{status}")
	@ApiResponse(responseCode = "200", description = "Users found by Vaccination Status, ", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UsersDto.class)))
	@Parameter(name = "status", required = true, description = "Status, only must be 0 for NOT_VACCINATED and 1 for VACCINATED")
	public UsersDto getUsersByVaccinationStatus(@PathVariable(required = true) Integer status,
			@RequestParam(required = false, defaultValue = "0") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit) {
		if (status != 0 && status != 1) {
			throw new BadRequestException("Status Invalid");
		}
		return userService.getUserByVaccinationStatus(
				status == 1 ? VaccinationStatus.VACCINATED : VaccinationStatus.NOT_VACCINATED,
				(page != null) ? page : 0, (limit != null) ? limit : 10);
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	@Operation(security = @SecurityRequirement(name = "bearer-key"))
	@GetMapping(value = "/vaccination/type/{type}")
	@ApiResponse(responseCode = "200", description = "Users found by Vaccination Status, ", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UsersDto.class)))
	@Parameter(name = "status", required = true, description = "Status, only must be 0 for NOT_VACCINATED and 1 for VACCINATED")
	public UsersDto getUsersByVaccinationType(@PathVariable(required = true) VaccineType type,
			@RequestParam(required = false, defaultValue = "0") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit) {
		return userService.getUserByVaccineType(type, (page != null) ? page : 0, (limit != null) ? limit : 10);
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	@Operation(security = @SecurityRequirement(name = "bearer-key"))
	@GetMapping(value = "/vaccination/date/{start}/{end}")
	@ApiResponse(responseCode = "200", description = "Users found by Vaccination Status, ", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UsersDto.class)))
	@Parameter(name = "status", required = true, description = "Status, only must be 0 for NOT_VACCINATED and 1 for VACCINATED")
	public UsersDto getUsersByVaccinationDate(
			@PathVariable(required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
			@PathVariable(required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
			@RequestParam(required = false, defaultValue = "0") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit) {
		return userService.getUserByVaccinationDate(start, end, (page != null) ? page : 0,
				(limit != null) ? limit : 10);
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	@Operation(security = @SecurityRequirement(name = "bearer-key"))
	@DeleteMapping("/{id}")
	@ApiResponse(responseCode = "200", description = "Delete User", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UsersDto.class)))
	public UserDto delete(@PathVariable Long id) {
		return userService.deleteUser(id);
	}
}
