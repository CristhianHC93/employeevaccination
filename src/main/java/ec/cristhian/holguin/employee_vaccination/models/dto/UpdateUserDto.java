package ec.cristhian.holguin.employee_vaccination.models.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

public class UpdateUserDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotEmpty
	@JsonProperty("first_name")
	private String firstName;

	@NotEmpty
	@JsonProperty("last_name")
	private String lastName;

	@NotNull
	@Temporal(TemporalType.DATE)
	@JsonProperty("birth_date")
	@Schema(description = "Format date yyyy-MM-dd")
	@Past(message = "date of birth must be less than today")
	@JsonFormat(pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthDate;

	@NotEmpty
	@JsonProperty("email")
	@Email
	private String email;

	@NotEmpty
	@JsonProperty("address")
	private String address;

	@NotEmpty
	@JsonProperty("mobile")
	private String mobile;

	@Schema(required = false, description = "password is optional")
	@JsonProperty("password")
	private String password;

	@NotNull
	@Min(0)
	@Max(1)
	@Schema(required = true, allowableValues = { "0", "1" })
	@JsonProperty("vaccination_status")
	private Integer vaccinationStatus;

	@JsonProperty("vaccination")
	@Nullable
	private VaccinationDto vaccination;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getVaccinationStatus() {
		return vaccinationStatus;
	}

	public void setVaccinationStatus(Integer vaccinationStatus) {
		this.vaccinationStatus = vaccinationStatus;
	}

	public VaccinationDto getVaccination() {
		return vaccination;
	}

	public void setVaccination(VaccinationDto vaccination) {
		this.vaccination = vaccination;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UpdateUserDto [firstName=" + firstName + ", lastName=" + lastName + ", birthDate=" + birthDate
				+ ", email=" + email + ", address=" + address + ", mobile=" + mobile + ", vaccinationStatus="
				+ vaccinationStatus + ", vaccination=" + vaccination + "]";
	}

}
