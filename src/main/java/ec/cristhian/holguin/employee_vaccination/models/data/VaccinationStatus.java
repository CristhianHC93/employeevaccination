package ec.cristhian.holguin.employee_vaccination.models.data;

public enum VaccinationStatus {
	VACCINATED(1), NOT_VACCINATED(0);

	private final int value;

	private VaccinationStatus(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
