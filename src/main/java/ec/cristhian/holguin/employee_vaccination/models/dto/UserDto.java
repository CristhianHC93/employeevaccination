package ec.cristhian.holguin.employee_vaccination.models.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

import ec.cristhian.holguin.employee_vaccination.models.entity.Role;

public class UserDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("last_name")
	private String lastName;

	private String username;

	@JsonProperty("birth_date")
	private Date birthDate;

	private String email;

	private String address;

	private String mobile;

	@JsonProperty("vaccination_status")
	private Integer vaccinationStatus;

	private VaccinationDto vaccination;

	private List<Role> roles;

	/* GETTER AND SETTER */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getVaccinationStatus() {
		return vaccinationStatus;
	}

	public void setVaccinationStatus(Integer vaccinationStatus) {
		this.vaccinationStatus = vaccinationStatus;
	}

	public VaccinationDto getVaccination() {
		return vaccination;
	}

	public void setVaccination(VaccinationDto vaccination) {
		this.vaccination = vaccination;
	}

	@Override
	public String toString() {
		return "UserDto [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", username=" + username
				+ ", birthDate=" + birthDate + ", email=" + email + ", address=" + address + ", mobile=" + mobile
				+ ", vaccinationStatus=" + vaccinationStatus + ", vaccination=" + vaccination + ", roles=" + roles
				+ "]";
	}
}
