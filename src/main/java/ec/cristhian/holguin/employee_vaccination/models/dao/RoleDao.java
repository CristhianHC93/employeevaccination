package ec.cristhian.holguin.employee_vaccination.models.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import ec.cristhian.holguin.employee_vaccination.models.entity.Role;

public interface RoleDao extends CrudRepository<Role, Long> {
	public Optional<Role> findByAuthority(String authority);
}
