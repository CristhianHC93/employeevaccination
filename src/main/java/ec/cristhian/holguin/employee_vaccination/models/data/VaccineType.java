package ec.cristhian.holguin.employee_vaccination.models.data;

public enum VaccineType {
	SPUTNIK, ASTRAZENECA, PFIZER, JHONSON_JHONSON;
}
