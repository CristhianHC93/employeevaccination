package ec.cristhian.holguin.employee_vaccination.models.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateUserDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotEmpty
	@JsonProperty("identification_card")
	@Pattern(regexp = "^[0-9]{10}", message = "length must be 10 and only allow numbers")
	private String identificationCard;

	@NotEmpty
	@JsonProperty("first_name")
	private String firstName;

	@NotEmpty
	@JsonProperty("last_name")
	private String lastName;

	@NotEmpty
	@JsonProperty("email")
	@Email
	private String email;

	public String getIdentificationCard() {
		return identificationCard;
	}

	public void setIdentificationCard(String identificationCard) {
		this.identificationCard = identificationCard;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "CreateUserDto [identificationCard=" + identificationCard + ", firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + "]";
	}

}
