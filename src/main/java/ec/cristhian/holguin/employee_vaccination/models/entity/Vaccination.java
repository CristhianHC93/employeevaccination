package ec.cristhian.holguin.employee_vaccination.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "vaccinations")
public class Vaccination {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "vaccine_type", nullable = false)
	private String vaccineType;

	@Temporal(TemporalType.DATE)
	@Column(name = "vaccination_date", nullable = false)
	private Date vaccinationDate;

	@Column(name = "number_doses", nullable = false)
	private Integer numberDoses;
	
	@OneToOne(mappedBy = "vaccination")
    private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVaccineType() {
		return vaccineType;
	}

	public void setVaccineType(String vaccineType) {
		this.vaccineType = vaccineType;
	}

	public Date getVaccinationDate() {
		return vaccinationDate;
	}

	public void setVaccinationDate(Date vaccinationDate) {
		this.vaccinationDate = new Date(vaccinationDate.getTime() + (1000 * 60 * 60 * 24));
	}

	public Integer getNumberDoses() {
		return numberDoses;
	}

	public void setNumberDoses(Integer numberDoses) {
		this.numberDoses = numberDoses;
	}

	@Override
	public String toString() {
		return "Vaccination [id=" + id + ", vaccineType=" + vaccineType + ", vaccinationDate=" + vaccinationDate
				+ ", numberDoses=" + numberDoses + "]";
	}

}
