package ec.cristhian.holguin.employee_vaccination.models.data;

public enum UserStatus {
	CREATED(0), UPDATED(1);

	private final int value;

	private UserStatus(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}