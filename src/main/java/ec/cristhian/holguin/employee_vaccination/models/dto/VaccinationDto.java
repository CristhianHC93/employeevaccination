package ec.cristhian.holguin.employee_vaccination.models.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

public class VaccinationDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@NonNull
	@JsonProperty("vaccine_type")
	private ec.cristhian.holguin.employee_vaccination.models.data.VaccineType vaccineType;

	@NotNull
	@Schema(description = "Format date yyy-MM-dd")
	@PastOrPresent(message = "Vaccination date must be less than tomorrow or future")
	@JsonFormat()
	@DateTimeFormat()
	@JsonProperty("vaccination_date")
	private Date vaccinationDate;

	@NotNull
	@Positive()
	@JsonProperty("number_doses")
	private Integer numberDoses;

	public ec.cristhian.holguin.employee_vaccination.models.data.VaccineType getVaccineType() {
		return vaccineType;
	}

	public void setVaccineType(ec.cristhian.holguin.employee_vaccination.models.data.VaccineType vaccineType) {
		this.vaccineType = vaccineType;
	}

	public Date getVaccinationDate() {
		return vaccinationDate;
	}

	public void setVaccinationDate(Date vaccinationDate) {
		this.vaccinationDate = vaccinationDate;
	}

	public Integer getNumberDoses() {
		return numberDoses;
	}

	public void setNumberDoses(Integer numberDoses) {
		this.numberDoses = numberDoses;
	}

	@Override
	public String toString() {
		return "VaccinationDto [vaccineType=" + vaccineType + ", vaccinationDate=" + vaccinationDate + ", numberDoses="
				+ numberDoses + "]";
	}

}
