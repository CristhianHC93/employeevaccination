package ec.cristhian.holguin.employee_vaccination.models.dto;

import java.io.Serializable;
import java.util.List;

public class UsersDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<UserDto> items;
	private MetaDto meta;

	public UsersDto(List<UserDto> items, MetaDto meta) {
		this.items = items;
		this.meta = meta;
	}

	public List<UserDto> getItems() {
		return items;
	}

	public void setItems(List<UserDto> items) {
		this.items = items;
	}

	public MetaDto getMeta() {
		return meta;
	}

	public void setMeta(MetaDto meta) {
		this.meta = meta;
	}

}
