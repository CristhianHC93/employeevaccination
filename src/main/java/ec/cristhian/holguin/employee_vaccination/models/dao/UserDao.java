package ec.cristhian.holguin.employee_vaccination.models.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.cristhian.holguin.employee_vaccination.models.entity.Role;
import ec.cristhian.holguin.employee_vaccination.models.entity.User;

public interface UserDao extends JpaRepository<User, Long> {
	public Optional<User> findByUsername(String username);
	public List<User> findByVaccinationStatus(Integer vaccinationStatus, Pageable pageable);
	public int countAllByVaccinationStatus(Integer vaccinationStatus);

	@Query(value ="SELECT u FROM User u where u.vaccination.vaccineType = :type")
	public List<User> findByVaccineType(String type, Pageable pageable);
	@Query(value ="SELECT count(u) FROM User u where u.vaccination.vaccineType = :type")
	public int countByVaccineType(String type);

	
	public List<User> findAllByRolesIn(List<Role> roles, Pageable pageable);
	public int countAllByRolesIn(List<Role> roles);
	
	@Query(value ="SELECT u FROM User u where u.vaccination.vaccinationDate BETWEEN :start and :end")
	public List<User> findByVaccinationDate(Date start,Date end, Pageable pageable);
	
	@Query(value ="SELECT count(u) FROM User u where u.vaccination.vaccinationDate BETWEEN :start and :end")
	public int countAllByVaccinationDate(Date start,Date end);
}
