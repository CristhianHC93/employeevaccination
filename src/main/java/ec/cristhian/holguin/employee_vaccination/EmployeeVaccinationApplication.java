package ec.cristhian.holguin.employee_vaccination;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class EmployeeVaccinationApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeVaccinationApplication.class, args);
	}

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	public void run(String... args) throws Exception {
		String password = "12345";
		System.out.println(passwordEncoder.encode(password));
	}
}