package ec.cristhian.holguin.employee_vaccination.auth.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.cristhian.holguin.employee_vaccination.service.UserService;

@Service("jpaUserDetailsService")
public class JpaUserDetailsService implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		ec.cristhian.holguin.employee_vaccination.models.entity.User user = userService.getUserByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User: " + username + " does not exist!");
		}

		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		user.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getAuthority())));
		if (authorities.isEmpty()) {
			throw new UsernameNotFoundException("Error Login: user '" + username + "' does not have assigned roles!");
		}
		return new User(user.getUsername(), user.getPassword(), true, true, true, true, authorities);
	}

}
