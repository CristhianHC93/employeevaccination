package ec.cristhian.holguin.employee_vaccination.exception;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityNotFoundException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import ec.cristhian.holguin.employee_vaccination.exception.exceptions.BadRequestException;
import ec.cristhian.holguin.employee_vaccination.exception.exceptions.NotFoundException;

@RestControllerAdvice
public class ExceptionHandlers {

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {

		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors()
				.forEach((error) -> errors.put(((FieldError) error).getField(), error.getDefaultMessage()));
		return errors;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(DataIntegrityViolationException.class)
	public Map<String, Object> dataIntegrityViolationException(DataIntegrityViolationException e) {
		return setMap(e);
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(NotFoundException.class)
	public Map<String, Object> notFoundException(NotFoundException e) {
		return setMap(e);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(BadRequestException.class)
	public Map<String, Object> badRequestException(BadRequestException e) {
		return setMap(e);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public Map<String, Object> methodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
		return setMap(e);
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(EntityNotFoundException.class)
	public Map<String, Object> methodEntityNotFoundException(EntityNotFoundException e) {
		return setMap(e);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public Map<String, Object> methodHttpMessageNotReadableException(HttpMessageNotReadableException e) {
		return setMap(e);
	}
	
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ExceptionHandler(AccessDeniedException.class)
	public Map<String, Object> methodAccessDeniedException(AccessDeniedException e) {
		return setMap(e);
	}

	private Map<String, Object> setMap(Exception e) {
		return Map.of("timestamp", LocalDateTime.now(), "message",
				(e.getCause() != null)
						? (e.getCause().getCause() != null) ? e.getCause().getCause().getMessage()
								: e.getMessage()
						: e.getMessage());
	}
}
