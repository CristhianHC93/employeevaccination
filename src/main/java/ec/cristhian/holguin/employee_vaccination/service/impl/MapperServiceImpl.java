package ec.cristhian.holguin.employee_vaccination.service.impl;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import ec.cristhian.holguin.employee_vaccination.service.MapperService;

@Service("mapper_service")
public class MapperServiceImpl implements MapperService {

	private ModelMapper modelMapper = new ModelMapper();

	@Override
	public <T> List<?> mapList(List<?> source, Function<Object, T> function) {
		return source.stream().map(element -> function.apply(element)).collect(Collectors.toList());
	}

	@Override
	public <T> Function<Object, T> mapObject(Class<T> targetClass) {
		return (x) -> modelMapper.map(x, targetClass);
	}

}
