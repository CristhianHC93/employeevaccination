package ec.cristhian.holguin.employee_vaccination.service;

import java.util.List;
import java.util.function.Function;

public interface MapperService {
	public <T> List<?> mapList(List<?> source, Function<Object, T> function); 
	public <T> Function<Object, T> mapObject(Class<T> targetClass);
}
