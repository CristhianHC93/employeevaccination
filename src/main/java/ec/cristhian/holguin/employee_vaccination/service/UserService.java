package ec.cristhian.holguin.employee_vaccination.service;

import java.util.Date;

import ec.cristhian.holguin.employee_vaccination.models.data.RoleName;
import ec.cristhian.holguin.employee_vaccination.models.data.VaccinationStatus;
import ec.cristhian.holguin.employee_vaccination.models.data.VaccineType;
import ec.cristhian.holguin.employee_vaccination.models.dto.CreateUserDto;
import ec.cristhian.holguin.employee_vaccination.models.dto.UpdateUserDto;
import ec.cristhian.holguin.employee_vaccination.models.dto.UserDto;
import ec.cristhian.holguin.employee_vaccination.models.dto.UsersDto;
import ec.cristhian.holguin.employee_vaccination.models.entity.User;

public interface UserService {
	public User getUserByUsername(String username);
	public UserDto createUser(CreateUserDto createUserDto);
	public UserDto updateUser(UpdateUserDto updateUserDto,Long id);
	public UsersDto getUserByRole(RoleName role, int page, int limit);
	public UserDto deleteUser(Long id);
	
	public UsersDto getUserByVaccinationStatus(VaccinationStatus vaccinationStatus, int page, int limit);
	public UsersDto getUserByVaccineType(VaccineType vaccineType, int page, int limit);
	public UsersDto getUserByVaccinationDate(Date start ,Date end, int page, int limit);
}
