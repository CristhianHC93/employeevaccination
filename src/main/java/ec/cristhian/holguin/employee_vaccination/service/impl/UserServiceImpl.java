package ec.cristhian.holguin.employee_vaccination.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.cristhian.holguin.employee_vaccination.exception.exceptions.BadRequestException;
import ec.cristhian.holguin.employee_vaccination.exception.exceptions.NotFoundException;
import ec.cristhian.holguin.employee_vaccination.models.dao.RoleDao;
import ec.cristhian.holguin.employee_vaccination.models.dao.UserDao;
import ec.cristhian.holguin.employee_vaccination.models.data.RoleName;
import ec.cristhian.holguin.employee_vaccination.models.data.UserStatus;
import ec.cristhian.holguin.employee_vaccination.models.data.VaccinationStatus;
import ec.cristhian.holguin.employee_vaccination.models.data.VaccineType;
import ec.cristhian.holguin.employee_vaccination.models.dto.CreateUserDto;
import ec.cristhian.holguin.employee_vaccination.models.dto.MetaDto;
import ec.cristhian.holguin.employee_vaccination.models.dto.UpdateUserDto;
import ec.cristhian.holguin.employee_vaccination.models.dto.UserDto;
import ec.cristhian.holguin.employee_vaccination.models.dto.UsersDto;
import ec.cristhian.holguin.employee_vaccination.models.dto.VaccinationDto;
import ec.cristhian.holguin.employee_vaccination.models.entity.Role;
import ec.cristhian.holguin.employee_vaccination.models.entity.User;
import ec.cristhian.holguin.employee_vaccination.models.entity.Vaccination;
import ec.cristhian.holguin.employee_vaccination.service.MapperService;
import ec.cristhian.holguin.employee_vaccination.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private UserDao userDao;
	private RoleDao roleDao;
	private MapperService mapperService;
	private BCryptPasswordEncoder passwordEncoder;

	public UserServiceImpl(UserDao userDao, RoleDao roleDao, MapperService mapperService,
			BCryptPasswordEncoder passwordEncoder) {
		this.userDao = userDao;
		this.roleDao = roleDao;
		this.mapperService = mapperService;
		this.passwordEncoder = passwordEncoder;
	}

	@Transactional(readOnly = true)
	@Override
	public User getUserByUsername(String username) {
		return userDao.findByUsername(username).orElseThrow(() -> new NotFoundException("User not found"));
	}

	@Override
	public UserDto createUser(CreateUserDto createUserDto) {
		User user = mapperService.mapObject(User.class).apply(createUserDto);
		user.setPassword(passwordEncoder.encode(user.getIdentificationCard()));
		user.setUsername(user.getIdentificationCard());
		user.setRoles(Arrays.asList(getRole(RoleName.ROLE_USER)));
		user.setCreateAt(new Date());
		user.setStatus(UserStatus.CREATED.getValue());
		return mapperService.mapObject(UserDto.class).apply(userDao.save(user));
	}

	@Transactional()
	@Override
	public UserDto updateUser(UpdateUserDto updateUserDto, Long id) {
		User user = setFieldToUser(updateUserDto, userDao.getById(id));
		if (updateUserDto.getVaccinationStatus() == VaccinationStatus.VACCINATED.getValue()) {
			if (updateUserDto.getVaccination() == null) {
				throw new BadRequestException("Vaccination information is necessary.");
			} else {
				setFieldVaccionationToUser(updateUserDto.getVaccination(), user);
				return mapperService.mapObject(UserDto.class).apply(userDao.save(user));
			}
		}else {
			user.setVaccination(null);
		}
		return mapperService.mapObject(UserDto.class).apply(userDao.save(user));
	}

	@Transactional(readOnly = true)
	@Override
	public UsersDto getUserByRole(RoleName role, int page, int limit) {
		return getUsers(getRole(role), page, limit, null);
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public UsersDto getUserByVaccinationStatus(VaccinationStatus vaccinationStatus, int page, int limit) {
		int totalItems = userDao.countAllByVaccinationStatus(vaccinationStatus.getValue());
		List<User> users = userDao.findByVaccinationStatus(vaccinationStatus.getValue(),
				PageRequest.of(page, limit, Sort.by("firstName")));
		List<UserDto> usersDto = (List<UserDto>) mapperService.mapList(users, mapperService.mapObject(UserDto.class));
		return new UsersDto(usersDto, new MetaDto(totalItems, users.size(), limit, (totalItems / limit), page));
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public UsersDto getUserByVaccineType(VaccineType vaccineType, int page, int limit) {
		int totalItems = userDao.countByVaccineType(vaccineType.toString());
		List<User> users = userDao.findByVaccineType(vaccineType.toString(),
				PageRequest.of(page, limit, Sort.by("firstName")));
		List<UserDto> usersDto = (List<UserDto>) mapperService.mapList(users, mapperService.mapObject(UserDto.class));
		return new UsersDto(usersDto, new MetaDto(totalItems, users.size(), limit, (totalItems / limit), page));
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public UsersDto getUserByVaccinationDate(Date start, Date end, int page, int limit) {
		int totalItems = userDao.countAllByVaccinationDate(start, end);
		List<User> users = userDao.findByVaccinationDate(start, end, PageRequest.of(page, limit, Sort.by("firstName")));
		List<UserDto> usersDto = (List<UserDto>) mapperService.mapList(users, mapperService.mapObject(UserDto.class));
		return new UsersDto(usersDto, new MetaDto(totalItems, users.size(), limit, (totalItems / limit), page));
	}

	@Transactional(readOnly = true)
	private Role getRole(RoleName role) {
		return roleDao.findByAuthority(role.toString()).orElseThrow(() -> new NotFoundException("Role not found"));
	}

	private User setFieldVaccionationToUser(VaccinationDto vaccinationDto, User user) {
		Vaccination vaccination = mapperService.mapObject(Vaccination.class).apply(vaccinationDto);
		if (user.getVaccination() != null) {
			vaccination.setId(user.getVaccination().getId());
		}
		user.setVaccination(vaccination);
		return user;
	}

	private User setFieldToUser(UpdateUserDto updateUserDto, User user) {
		user.setFirstName(updateUserDto.getFirstName());
		user.setLastName(updateUserDto.getLastName());
		user.setEmail(updateUserDto.getEmail());
		user.setBirthDate(updateUserDto.getBirthDate());
		user.setAddress(updateUserDto.getAddress());
		user.setMobile(updateUserDto.getMobile());
		user.setUpdateAt(new Date());
		user.setStatus(UserStatus.UPDATED.getValue());
		user.setVaccinationStatus(updateUserDto.getVaccinationStatus());
		if (updateUserDto.getPassword() != null && !updateUserDto.getPassword().isEmpty()) {
			user.setPassword(passwordEncoder.encode(updateUserDto.getPassword()));
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	private UsersDto getUsers(Role role, int page, int limit, Long userId) {
		int totalItems = userDao.countAllByRolesIn(Arrays.asList(role));
		List<User> users = userDao.findAllByRolesIn(Arrays.asList(role),
				PageRequest.of(page, limit, Sort.by("firstName")));
		List<UserDto> usersDto = (List<UserDto>) mapperService.mapList(users, mapperService.mapObject(UserDto.class));
		if (userId != null) {
			usersDto.removeIf(user -> user.getId() == userId);
		}
		return new UsersDto(usersDto, new MetaDto(totalItems, users.size(), limit, (totalItems / limit), page));
	}

	@Override
	@Transactional()
	public UserDto deleteUser(Long id) {
		User user = userDao.getById(id);
		userDao.delete(user);
		return mapperService.mapObject(UserDto.class).apply(user);
	}
}
