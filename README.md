# Api Employee Vaccination #

Credentials for admin user default 
username:admin 
password:12345.

### What is this repository for? ###
To manage staff vaccinations


### How do I get set up? ###

* Clone Repo with git clone https://CristhianHC93@bitbucket.org/CristhianHC93/employeevaccination.git
* Create database employee_vaccination and test_employee_vaccination in postgres
* Config file application.properties in src/main/resouces for conexion with database employee_vaccination
* Config file application.properties in src/test/resouces for conexion with database test_employee_vaccination
* Build and run project
* Link to swagger http://localhost:8080/doc
* Import file employee_vaccination.postman_collection.json in postman
* Execute POST http://localhost:8080/api/login and copy token, later you can taste the end points 